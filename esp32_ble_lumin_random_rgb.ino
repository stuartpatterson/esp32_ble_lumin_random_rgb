//https://github.com/nkolban/ESP32_BLE_Arduino/blob/master/src/BLEAdvertisedDevice.h
#include "BLEDevice.h"

String serverUUID = "a4:01:00:01:2f:01";
static BLEUUID serviceUUID("ffd5");
static BLEUUID characteristicUUID("ffd9");
bool connected = false;

BLEClient* pMyClient = BLEDevice::createClient();
BLEAddress address(serverUUID.c_str());
BLERemoteService* pMyRemoteService;
BLERemoteCharacteristic* pMyRemoteCharacteristic;

uint8_t rgb_color_data[] = { 0x56, 0x00, 0x00, 0x00, 0x01, 0xf0, 0xaa };

void setup() {
  BLEDevice::init("");
}

void loop() {
  static uint8_t r=0,g=0,b=0;
  
  if ( !connected ) {
    if ( pMyClient->connect(address) ) {
      connected = true;
      pMyRemoteService = pMyClient->getService(serviceUUID);
      pMyRemoteCharacteristic = pMyRemoteService->getCharacteristic(characteristicUUID);
    }
  }

  if ( connected ) {
    if ( pMyRemoteCharacteristic != NULL ) {
      if ( pMyRemoteCharacteristic->canWriteNoResponse() ) {

        r = random(0,256);
        g = random(0,256);
        b = random(0,256);
        
        rgb_color_data[1] = r;
        rgb_color_data[2] = g;
        rgb_color_data[3] = b;

        pMyRemoteCharacteristic->writeValue(rgb_color_data, sizeof(rgb_color_data));
        delay(500);
      }
    }
  }
}
